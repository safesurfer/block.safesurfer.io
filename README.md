# Block Page

Branded implementation of the [block page](https://gitlab.com/safesurfer/block-page). Hosted at [block.safesurfer.io](https://block.safesurfer.io).

