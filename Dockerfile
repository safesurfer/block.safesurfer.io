FROM golang:1.19-alpine AS build
WORKDIR /app
RUN apk add tzdata ca-certificates --no-cache
COPY go.* ./
COPY *.go ./
COPY assets assets
COPY favicon.ico ./
COPY index.html ./
COPY robots.txt ./
RUN adduser -D user
RUN CGO_ENABLED=0 go build -ldflags='-s -w' -o block-page-exec

FROM scratch
WORKDIR /app
ARG VERSION
ENV APP_VERSION=$VERSION
ENV PATH=/app
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /app/block-page-exec .
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
USER user
ENTRYPOINT ["/app/block-page-exec"]